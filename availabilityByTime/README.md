# availabilityByTime - Check if the offer opening times are between current time. If yes - add Tag is_active with value true to the offer.

This is first script written by myself. This is custom script modifying XML file. The logic is described below:

    - all of tags are rewrite to the new XML,

    - in opening_times tag there are opening hours for each day of the offer - we get the values and compare it with current time,

    - if current time is between opening hours the script add tag is_active with value true. If not the script add tag is_active with value false.
    

## Usage

```PHP
php index.php
